FROM python

COPY . /app

WORKDIR /app

RUN pip install --upgrade pip && pip install --no-cache-dir flask

EXPOSE 5000

CMD ["python3", "./app2.py"]