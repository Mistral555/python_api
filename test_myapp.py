import pytest
from app2 import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_hello(client):
    response = client.get('/hello')
    assert response.status_code == 200
    assert b"Hello, welcome to the ESBay User API" in response.data

def test_add(client):
    response = client.get('/api/add?a=2&b=3')
    assert response.status_code == 200
    assert b'"result":5' in response.data

def test_add_invalid_input(client):
    response = client.get('/api/add')
    assert response.status_code == 400
    assert b'"error":"Invalid input, please provide two numbers"' in response.data