from flask import Flask, request, redirect, url_for

app = Flask(__name__)


# redirected
@app.route('/hello/<name>')
def hello(name):
    return 'Hello World ' +str(name)

@app.route('/logout')
def logout():
    # return redirect('/connect')
    return 'Bye world'

@app.route('/connect')
def connect():
    return 'Connecting'


# two arguments
@app.route('/hello2')
def hello_world():
    p1 = request.args.get('p1')
    p2 = request.args.get('p2')
    return f"Je suis : {p1} {p2}"



# two parameters
@app.route('/hello3/<name>/<sname>')
def hello_world2(name,sname):
    return 'Hello World ' +str(name)+ ' '+ str(sname)




# url_for
@app.route('/hello4')
def hello_world3():
    return redirect(url_for('hello',name='zoubir'))

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5000)