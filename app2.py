from flask import Flask, request, jsonify
app = Flask(__name__)
@app.route('/hello')
def hello():
    return 'Hello, welcome to the ESBay User API\n'


@app.route('/api/add', methods=['GET'])
def add():
    number1 = request.args.get('a', type=int)
    number2 = request.args.get('b', type=int)
    if number1 is None or number2 is None:
        return jsonify({'error': 'Invalid input, please provide two numbers'}), 400
    result = number1 + number2
    return jsonify({'result': result})

@app.route('/api/sous', methods=['GET'])
def soustraction():
    number1 = request.args.get('a', type=int)
    number2 = request.args.get('b', type=int)
    if number1 is None or number2 is None:
        return jsonify({'error': 'Invalid input, please provide two numbers'}), 400
    result = number1 - number2
    return jsonify({'result': result})

@app.route('/api/mult', methods=['GET'])
def multiplication():
    number1 = request.args.get('a', type=int)
    number2 = request.args.get('b', type=int)
    if number1 is None or number2 is None:
        return jsonify({'error': 'Invalid input, please provide two numbers'}), 400
    result = number1 * number2
    return jsonify({'result': result})

@app.route('/api/div', methods=['GET'])
def division():
    number1 = request.args.get('a', type=int)
    number2 = request.args.get('b', type=int)
    if number1 is None or number2 is None:
        return jsonify({'error': 'Invalid input, please provide two numbers'}), 400
    if number2 == 0:
        return jsonify({'error': 'division par 0 impossible'}), 400
    else:
        result = number1 / number2
        return jsonify({'result': result})
    
@app.route('/api/square', methods=['GET'])
def square():
    number1 = request.args.get('a', type=int)
    number2 = request.args.get('b', type=int)
    if number1 is None or number2 is None:
        return jsonify({'error': 'Invalid input, please provide two numbers'}), 400
    else:
        result = number1 ** number2
        return jsonify({'result': result})


if __name__ == '__main__':
 app.run(debug=False, host='0.0.0.0', port=5000)